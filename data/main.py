#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import random
import json
import os

from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from config import TOKEN, WORKERS

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)

auth_users = dict()
DATA_PATH = '/home/user/user_db'

def write_data(user_id, passphrase, data):
    fname = '%s/%s_%s.json' % (DATA_PATH, user_id, passphrase)
    with open(fname, 'wt') as fd:
        json.dump(data, fd)

    return fname

def load_data(user_id, passphrase):
    data = {}
    fname = '%s/%s_%s.json' % (DATA_PATH, user_id, passphrase)
    with open(fname, 'rt') as fd:
        data = json.load(fd)

    return data

def remove_data(user_id, passphrase):
    fname = '%s/%s_%s.json' % (DATA_PATH, user_id, passphrase)
    if os.path.exists(fname):
        os.remove(fname)

def start_handler(bot, update):
    """Handler for the /start command."""
    bot.send_message(chat_id=update.effective_message.chat_id, text='Hey!')
    help_handler(bot, update)

def help_handler(bot, update):
    """Handler for the /help command."""
    message = ('I\'m a data saving bot, send me data and I will keep it for you!\n'
               'You can interact with me by sending these commands:\n\n'
               '/reg - Registration (create your database)\n'
               '/auth - Authentication (load your database)\n'
               '/deauth - Deauthentication (unload your database)\n'
               '/get - Get data\n'
               '/drop - Drop data\n'
               '/post - Post data')
    bot.send_message(chat_id=update.effective_message.chat_id, text=message)

def reg_handler(bot, update, args):
    """Handler for the /reg command."""
    user = update.effective_user
    user_id = user.id
    if len(args) != 0:
        passphrase = args[0]
        fname = '%s/%s_%s.json' % (DATA_PATH, user_id, passphrase)

        logger.info('Registration from %s (#%s) with %s' % (user.name, user_id, args[0]))

        if not os.path.exists(fname):
            write_data(user_id, passphrase, [])

            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                             text='Your database has successfully been created %s! Now use `/auth PASSPHRASE` to access it!' % (user.name))
        else:
            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                             text='An error occured while creating your database, make sure that it doesn\'t already exists!')
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='No passphrase provided, please register using `/reg PASSPHRASE` command!')

def auth_handler(bot, update, args):
    """Handler for the /auth command."""
    user = update.effective_user
    user_id = user.id
    if len(args) != 0:
        passphrase = args[0]
        try:
            data = load_data(user_id, passphrase)
        except FileNotFoundError:
            auth_result = False
        else:
            auth_result = True
            auth_users[user_id] = {'passphrase': passphrase, 'data': data}

        logger.info('Authentication from %s (#%s) with %s: %s' % (user.name, user_id, passphrase, 'success' if auth_result else 'failed'))

        if auth_result:
            bot.send_message(chat_id=update.effective_message.chat_id, text='Welcome %s!' % (user.name))
        else:
            bot.send_message(chat_id=update.effective_message.chat_id, text='Sorry %s, the authentication failed :/' % (user.name))
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='No passphrase provided, please authenticate using `/auth PASSPHRASE` command!')

def deauth_handler(bot, update):
    """Handler for the /drop command."""
    user = update.effective_user
    user_id = user.id
    if user_id in auth_users:
        auth_users.pop(user_id)
        logger.info('%s (#%s) disconnected!' % (user.name, user_id))
        bot.send_message(chat_id=update.effective_message.chat_id, text='Bye %s!' % (user.name))
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not authenticated! Please authenticate or register using either `/auth PASSPHRASE` or `/reg PASSPHRASE` command!')

def get_handler(bot, update):
    """Handler for the /get command."""
    user = update.effective_user
    user_id = user.id
    if user_id in auth_users:
        logger.info('%s (#%s) asked for its data' % (user.name, user_id))

        if len(auth_users[user_id]['data']) != 0:
            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                             text='*Your data:*\n\n%s' % ('\n'.join(auth_users[user_id]['data'])))
        else:
            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                             text='*No data*')
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not authenticated! Please authenticate or register using either `/auth PASSPHRASE` or `/reg PASSPHRASE` command!')

def drop_callback_handler(bot, update):
    """Handler for the drop confirm menu."""
    user = update.effective_user
    user_id = user.id
    if user_id in auth_users:
        user_choice = update.callback_query.data.split(':', 1)[1]

        if user_choice == 'yes':
            remove_data(user_id, auth_users[user_id]['passphrase'])
            update.callback_query.answer('Your database has been deleted')
            logger.info('%s (#%s) data has been dropped' % (user.name, user_id))
            deauth_handler(bot, update)
        else:
            update.callback_query.answer('Operation aborted')
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not authenticated! Please authenticate or register using either `/auth PASSPHRASE` or `/reg PASSPHRASE` command!')

def drop_handler(bot, update):
    """Handler for the /drop command."""
    user = update.effective_user
    user_id = user.id
    if user_id in auth_users:
        logger.info('%s (#%s) asked for its data to be dropped' % (user.name, user_id))

        button_list = [InlineKeyboardButton(text='yes', callback_data='drop:yes'),
                       InlineKeyboardButton(text='no', callback_data='drop:no')]
        reply_markup = InlineKeyboardMarkup([button_list])
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.HTML,
                         text='Are you sure you want to drop your database?', reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not authenticated! Please authenticate or register using either `/auth PASSPHRASE` or `/reg PASSPHRASE` command!')

def post_handler(bot, update, args):
    """Handler for the /post command."""
    user = update.effective_user
    user_id = user.id
    if user_id in auth_users:
        if len(args) != 0:
            data = ' '.join(args)

            logger.info('%s (#%s) posted data: %s' % (user.name, user_id, ' '.join(args)))

            auth_users[user_id]['data'].append(data)
            write_data(user_id, auth_users[user_id]['passphrase'], auth_users[user_id]['data'])
            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                             text='You data has been saved!')
        else:
            bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                            text='No data provided, please post data using `/post DATA TO SAVE` command!')
    else:
        bot.send_message(chat_id=update.effective_message.chat_id, parse_mode=ParseMode.MARKDOWN,
                         text='You\'re not authenticated! Please authenticate or register using either `/auth PASSPHRASE` or `/reg PASSPHRASE` command!')

def unknown_command_handler(bot, update):
    """Unknown command handler."""
    user = update.effective_user
    user_id = user.id
    logger.info('Unknown command from %s (#%s): %s' % (user.name, user_id, update.effective_message.text))
    bot.send_message(chat_id=update.effective_message.chat_id, text='Sorry, I didn\'t understand that command. /help')

def unknown_text_handler(bot, update):
    """Unknown text handler."""
    user = update.effective_user
    user_id = user.id
    logger.info('Unknown text from %s (#%s): %s' % (user.name, user_id, update.effective_message.text))
    bot.send_message(chat_id=update.effective_message.chat_id, text='What are you telling me? /help')

def main():
    updater = Updater(token=TOKEN, workers=WORKERS)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start_handler))
    dispatcher.add_handler(CommandHandler('help', help_handler))
    dispatcher.add_handler(CommandHandler('reg', reg_handler, pass_args=True))
    dispatcher.add_handler(CommandHandler('auth', auth_handler, pass_args=True))
    dispatcher.add_handler(CommandHandler('deauth', deauth_handler))
    dispatcher.add_handler(CommandHandler('get', get_handler))
    dispatcher.add_handler(CommandHandler('drop', drop_handler))
    dispatcher.add_handler(CallbackQueryHandler(drop_callback_handler, pattern=r'^drop:\w+$'))
    dispatcher.add_handler(CommandHandler('post', post_handler, pass_args=True))
    dispatcher.add_handler(MessageHandler(Filters.command, unknown_command_handler))
    dispatcher.add_handler(MessageHandler(Filters.text, unknown_text_handler))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
